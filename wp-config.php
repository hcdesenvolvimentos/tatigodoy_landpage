<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'projetos_tatigodoy_landpage');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#iLP,5$|nb6n?PnyyUOnx<!>;;t^cY_ZRa22eGl[zk,QmTjlfIv<usf_L~%=8iHz');
define('SECURE_AUTH_KEY',  '|uxfM6Ih25E::_;4P^30@dpbL3g;r2?jw!ESr.E1&-I0hRD-N_GruK9w}3Kwj/C.');
define('LOGGED_IN_KEY',    'k4js7jp#xCkhb~YwO#q>-tR=?0NJ<@IkHDr2dG*qy<bSs* BMU$Z.CkfudA|PRv:');
define('NONCE_KEY',        'A`^apX,u&PL|b7|5yHrb?z;}6Xa6d8d<D-b6qsM<W{B|e1`>7qq30mYVh,Z xVOz');
define('AUTH_SALT',        'sd)o<zbnA!Y-#JUj8,h}se}`AT@bhFG~2gWc?m{Z%v@8L1Av-Di/%*V7PX!pn>wS');
define('SECURE_AUTH_SALT', 'q(y+`XzRRWLc}LgJ^1Ni{M_ctRmu&.{p%AU.U}@e6R#3jJ dz8h}c51dM~}QhHmD');
define('LOGGED_IN_SALT',   't)kihlT98{&xmy!r%~bLNkZFHLJ7#ZD|WafdGHZYbbqq*|%34FWTb(p1eNL:t=DE');
define('NONCE_SALT',       ';9i.Y}<yGk[!&Pye@dJp.!6Jmu&5$1T)yI:[7U=w.YXzYPi*&.ar#/BfEz-$Vi-4');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'tg_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
