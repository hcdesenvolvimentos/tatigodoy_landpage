<?php

/**
 @wordpress-plugin
 * Plugin Name: Tati Godoy Blog
 * Version:     0.1
 * Plugin URI:  http://hcdesenvolvimentos.com.br 
 * Description: Controle base do tema Tati Godoy Blog.
 * Author:       Agência HC Desenvolvimentos
 * Author URI:   http://hcdesenvolvimentos.com.br 
 * Text Domain: hcdesenvolvimentos-base
 * Domain Path: /languages/
 * License:     GPL2
 */


function baseTatigodoy () {

	// TIPOS DE CONTEÚDO
	conteudosTatigodoy();

	metaboxesTatigodoy();

	tipoDepoimentos();
}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosTatigodoy (){

		// TIPOS DE CONTEÚDO
		tipoCursos();

		// TIPOS DE CONTEÚDO
		tipoGanhos();

		//TIPOS DE CONTEÚDO
		//tipoProximosCursos();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
				$titulo = 'Título do destaque';
				break;

				case 'team':
				$titulo = 'Enter member name here';
				break;
				
				default:
				break;
			}

			return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoCursos() {

		$rotulosCurso = array(
			'name'               => 'Cursos',
			'singular_name'      => 'curso',
			'menu_name'          => 'Cursos',
			'name_admin_bar'     => 'Cursos',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo curso',
			'new_item'           => 'Novo curso',
			'edit_item'          => 'Editar curso',
			'view_item'          => 'Ver curso',
			'all_items'          => 'Todos os cursos',
			'search_items'       => 'Buscar curso',
			'parent_item_colon'  => 'Dos cursos',
			'not_found'          => 'Nenhum curso cadastrado.',
			'not_found_in_trash' => 'Nenhum curso na lixeira.'
		);

		$argsCurso 	= array(
			'labels'             => $rotulosCurso,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-images-alt',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'cursos' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail','editor')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('curso', $argsCurso);

	}

	function tipoGanhos() {

		$rotulosGanhos = array(
			'name'               => 'Ganhos',
			'singular_name'      => 'ganho',
			'menu_name'          => 'Ganhos',
			'name_admin_bar'     => 'Ganhos',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo ganho',
			'new_item'           => 'Novo ganho',
			'edit_item'          => 'Editar ganho',
			'view_item'          => 'Ver ganho',
			'all_items'          => 'Todos os ganhos',
			'search_items'       => 'Buscar ganho',
			'parent_item_colon'  => 'Dos ganhos',
			'not_found'          => 'Nenhum ganho cadastrado.',
			'not_found_in_trash' => 'Nenhum ganho na lixeira.'
		);

		$argsGanhos 	= array(
			'labels'             => $rotulosGanhos,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-external',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'ganhos' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','editor','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('ganhos', $argsGanhos);

	}

	function tipoProximosCursos() {

		$rotulosProximosCursos = array(
			'name'               => 'Próximos cursos',
			'singular_name'      => 'próximo curso',
			'menu_name'          => 'Próximos cursos',
			'name_admin_bar'     => 'Próximos cursos',
			'add_new'            => 'Adicionar novo curso',
			'add_new_item'       => 'Adicionar novo curso',
			'new_item'           => 'Novo curso',
			'edit_item'          => 'Editar curso',
			'view_item'          => 'Ver curso',
			'all_items'          => 'Todos os curso',
			'search_items'       => 'Buscar curso',
			'parent_item_colon'  => 'Dos próximos cursos',
			'not_found'          => 'Nenhum cursos cadastrado.',
			'not_found_in_trash' => 'Nenhum cursos na lixeira.'
		);

		$argsProximosCursos 	= array(
			'labels'             => $rotulosProximosCursos,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-calendar-alt',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'proximos-cursos' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('proximos-cursos', $argsProximosCursos);

	}

	// CUSTOM POST TYPE DEPOIMENTOS
	function tipoDepoimentos() {

		$rotulosDepoimentos = array(
			'name'               => 'Depoimentos',
			'singular_name'      => 'depoimento',
			'menu_name'          => 'Depoimentos',
			'name_admin_bar'     => 'Depoimentos',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo depoimento',
			'new_item'           => 'Novo depoimento',
			'edit_item'          => 'Editar depoimento',
			'view_item'          => 'Ver depoimento',
			'all_items'          => 'Todos os depoimentos',
			'search_items'       => 'Buscar depoimento',
			'parent_item_colon'  => 'Dos depoimentos',
			'not_found'          => 'Nenhum depoimento cadastrado.',
			'not_found_in_trash' => 'Nenhum depoimento na lixeira.'
		);

		$argsDepoimentos = array(
			'labels'             => $rotulosDepoimentos,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-format-quote',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'depoimento' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','editor','thumbnail', 'excerpt')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('depoimento', $argsDepoimentos);

	}


	/****************************************************
	* TAXONOMIA
	*****************************************************/



    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesTatigodoy(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

	function registraMetaboxes( $metaboxes ){

		$prefix = 'Tatigodoy_';

		// METABOX DE CURSOS
		$metaboxes[] = array(
			'id'			=> 'cursosMetabox',
			'title'			=> 'Detalhe do curso',
			'pages' 		=> array( 'curso' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

				array(
					'name'  => 'Subtitutlo:',
					'id'    => "{$prefix}curso_subtitulo",
					'desc'  => '',
					'type'  => 'text',
				),
				array(
					'name'  => 'Frases do curso:',
					'id'    => "{$prefix}curso_frase",
					'desc'  => '',
					'type'  => 'text',
					'clone'  => true,
				),
				array(
					'name'  => 'ID Vídeo do curso:',
					'id'    => "{$prefix}curso_video",
					'desc'  => '',
					'type'  => 'text',
				),
			),
		);

		// METABOX DE CURSOS
		$metaboxes[] = array(
			'id'			=> 'cursosMetabox2',
			'title'			=> 'Vídeos depoimentos',
			'pages' 		=> array( 'curso' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

			
				array(
					'name'  => 'Vídeos depoimentos:',
					'id'    => "{$prefix}curso_depoimentos",
					'desc'  => 'Cadastrar nome do vídeo | ID: Testemunhos Sobre Tati Godoy | YMWMS',
					'type'  => 'textarea',
					'clone'  => true,
				),
				
			),
		);

		// METABOX DE CURSOS
		$metaboxes[] = array(
			'id'			=> 'cursosMetabox3',
			'title'			=> 'Ganhos',
			'pages' 		=> array( 'curso' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

			
				array(
					'name'  	 => 'Ganhos: ',
					'id'    	 => "{$prefix}curso_ganhos",
					'desc'  	 => '',
					'type'  	 => 'post',
					'post_type'  => 'ganhos',
					'field_type' => 'select',
					'multiple' => true,
				),
				
			),
		);

		// METABOX DE CURSOS
		$metaboxes[] = array(
			'id'			=> 'cursosMetabox4',
			'title'			=> 'Informações do curso',
			'pages' 		=> array( 'curso' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

			
				array(
					'name'  	 => 'Endereço: ',
					'id'    	 => "{$prefix}curso_endereco",
					'desc'  	 => '',
					'type'  	 => 'text',
				),

				array(
					'name'  	 => 'Data/Horário: ',
					'id'    	 => "{$prefix}curso_data",
					'desc'  	 => '',
					'type'  	 => 'text',
				),
				array(
					'name'  	 => 'Materiais: ',
					'id'    	 => "{$prefix}curso_material",
					'desc'  	 => '',
					'type'  	 => 'textarea',
				),
				array(
					'name'  	 => 'Coffees breaks: ',
					'id'    	 => "{$prefix}curso_coffeesbreaks",
					'desc'  	 => '',
					'type'  	 => 'textarea',
				),
				
			),
		);
		
		// METABOX DE CURSOS
		$metaboxes[] = array(
			'id'			=> 'cursosMetabox5',
			'title'			=> 'Informações de preço 2',
			'pages' 		=> array( 'curso' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

			
				array(
					'name'  	 => 'Preço parcelado: ',
					'id'    	 => "{$prefix}curso_parcelado",
					'desc'  	 => '5X',
					'type'  	 => 'text',
				),

				array(
					'name'  	 => 'Preço: ',
					'id'    	 => "{$prefix}curso_preco",
					'desc'  	 => '320,00',
					'type'  	 => 'text',
				),
				array(
					'name'  	 => 'Link: ',
					'id'    	 => "{$prefix}curso_link",
					'desc'  	 => '',
					'type'  	 => 'text',
				),
				
			),
		);

		// METABOX DE CURSOS
		$metaboxes[] = array(
			'id'			=> 'cursosMetabox6',
			'title'			=> 'Informações de preço á Vista',
			'pages' 		=> array( 'curso' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

			
				array(
					'name'  	 => 'Preço parcelado: ',
					'id'    	 => "{$prefix}curso_parcelado_avista",
					'desc'  	 => 'Á VISTA',
					'type'  	 => 'text',
				),

				array(
					'name'  	 => 'Preço: ',
					'id'    	 => "{$prefix}curso_preco_avista",
					'desc'  	 => '320,00',
					'type'  	 => 'text',
				),
				array(
					'name'  	 => 'Link: ',
					'id'    	 => "{$prefix}curso_link_avista",
					'desc'  	 => '',
					'type'  	 => 'text',
				),
				
			),
		);

		// METABOX DE CURSOS
		$metaboxes[] = array(
			'id'			=> 'cursosMetabox7',
			'title'			=> 'Grupo Whatsap',
			'pages' 		=> array( 'curso' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(
				array(
					'name'  	 => 'Link: ',
					'id'    	 => "{$prefix}curso_Whatsap",
					'desc'  	 => '',
					'type'  	 => 'text',
				),
			),
		);

		// METABOX DE CURSOS
		$metaboxes[] = array(
			'id'			=> 'cursosMetabox8',
			'title'			=> 'Data final de  promoção',
			'pages' 		=> array( 'curso' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(
				array(
					'name'  	 => 'Data: ',
					'id'    	 => "{$prefix}curso_promocao",
					'desc'  	 => 'Colocar exatamente assim:  Dec 25, 2018 23:00:00',
					'type'  	 => 'text',
					'placeholder'  	 => 'Dec 25, 2018 00:00:00',
				),
			),
		);

		// METABOX DE CURSOS
		$metaboxes[] = array(
			'id'			=> 'cursosMetaboxNovo',
			'title'			=> 'Informações de preço 1',
			'pages' 		=> array( 'curso' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

			
				array(
					'name'  	 => 'Preço parcelado: ',
					'id'    	 => "{$prefix}curso_parcelado_novo",
					'desc'  	 => '',
					'type'  	 => 'text',
				),

				array(
					'name'  	 => 'Preço: ',
					'id'    	 => "{$prefix}curso_preco_novo",
					'desc'  	 => '',
					'type'  	 => 'text',
				),
				array(
					'name'  	 => 'Link: ',
					'id'    	 => "{$prefix}curso_link_novo",
					'desc'  	 => '',
					'type'  	 => 'text',
				),
				
			),
		);
		// METABOX DE CURSOS
		$metaboxes[] = array(
			'id'			=> 'cursosMetaboxNovo2',
			'title'			=> 'Script Facebook',
			'pages' 		=> array( 'curso' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

			
				array(
					'name'  	 => 'Script Facebook: ',
					'id'    	 => "{$prefix}script_facebook",
					'desc'  	 => '',
					'type'  	 => 'text',
				),
			),
		);


		// METABOX DE PRÓXIMOS CURSOS
		$metaboxes[] = array(
			'id'			=> 'proximosCursos',
			'title'			=> 'Atributos do próximo curso',
			'pages' 		=> array( 'curso' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(
				array(
					'name'  	 => 'Cidade: ',
					'id'    	 => "{$prefix}prox_curso_cidade",
					'desc'  	 => '',
					'type'  	 => 'text',
				),
				array(
					'name'  	 => 'Local: ',
					'id'    	 => "{$prefix}prox_curso_local",
					'desc'  	 => '',
					'type'  	 => 'text',
				),
				array(
					'name'  	 => 'Data do curso: ',
					'id'    	 => "{$prefix}prox_curso_data",
					'desc'  	 => '',
					'type'  	 => 'date',
					'js_options' => array(
						'dateFormat'      => 'dd-mm-yy',
						'showButtonPanel' => false,
					),
				),
				array(
					'name'  	 => 'Hora: ',
					'id'    	 => "{$prefix}prox_curso_hora",
					'desc'  	 => '',
					'type'  	 => 'time',
					'js_options' => array(
						'stepMinute'      => 1,
						'controlType'     => 'select',
						'showButtonPanel' => false,
						'oneLine'         => true,
					),
				),
				array(
					'name'  	 => 'Imagem de fundo: ',
					'id'    	 => "{$prefix}prox_curso_fundo",
					'desc'  	 => '',
					'type'  	 => 'image_advanced',
					'max_file_uploads' => 1,
				),
				array(
					'name'  	 => 'Imagens do local: ',
					'id'    	 => "{$prefix}prox_curso_imagens",
					'desc'  	 => '',
					'type'  	 => 'image_advanced',
					'max_file_uploads' => 2,
				),
			),
		);

		return $metaboxes;
	}

	function metaboxjs(){

		global $post;
		$template = get_post_meta($post->ID, '_wp_page_template', true);
		$template = explode('/', $template);
		$template = explode('.', $template[1]);
		$template = $template[0];

		if($template != ''){
			wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
		}
	}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesTatigodoy(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerTatigodoy(){

		if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseTatigodoy');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

		baseTatigodoy();

		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );