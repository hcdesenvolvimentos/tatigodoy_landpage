<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Tati_Godoy_Cursos
 */
global $configuracao;
$contato_whatsapp =  $configuracao['contato_whatsapp'];
$contato = str_replace(array( '(', ')', '-' ), '',$contato_whatsapp);

?>

<footer class="rodape">
	<div class="areaWhatsapp">
		<a href="https://api.whatsapp.com/send?1=pt_BR&phone=55<?php echo $contato ?>" target="_blank">
			<i class="fab fa-whatsapp"></i>
			<p>ATENDIMENTO VIA WHATSAPP: <?php echo $contato_whatsapp ?></p>
			<small>Para maiores informações e esclarecimentos sobre o curso. </small>
		</a>
	</div>

	<div class="redesSociais">
		<ul>
			<li><a href="https://www.facebook.com/FacilitaOrganizacao/" target="_blank"><i class="fab fa-facebook-f"></i> Facebook</a></li>
			<li><a href="https://plus.google.com/u/0/103384274064249926821" target="_blank"><i class="fab fa-google-plus-g"></i> Google +</a></li>
			<li><a href="https://www.instagram.com/tatigodoyfacilita/" target="_blank"><i class="fab fa-instagram"></i> Instagram</a></li>
			<li><a href="https://www.youtube.com/channel/UCBlBXUL1EuSFQX_u1LJ_yNw" target="_blank"><i class="fab fa-youtube"></i> Youtube</a></li>
			<li><a href="mailto:cursos@facilitalimpeza.com.br"><i class="far fa-envelope"></i> E-mail</a></li>
		</ul>
	</div>
	<div class="copyright">
		<div class="containerFull">
			<hr>
			<p>Todos os direitos reservados</p>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
