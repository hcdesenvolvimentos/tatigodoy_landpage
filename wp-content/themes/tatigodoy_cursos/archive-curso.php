<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Tati_Godoy_Cursos
 */
global $configuracao;
get_header();
?>

<?php 
	if ( have_posts() ) : while ( have_posts() ) :the_post();
	$curso_frase = rwmb_meta('Tatigodoy_curso_frase');
		$curso_ganhos = rwmb_meta('Tatigodoy_curso_ganhos');
		$curso_endereco = rwmb_meta('Tatigodoy_curso_endereco');

		$curso_data = rwmb_meta('Tatigodoy_curso_data');
		$curso_material = rwmb_meta('Tatigodoy_curso_material');
		$curso_coffeesbreaks = rwmb_meta('Tatigodoy_curso_coffeesbreaks');

		// curso_preco
		$curso_parcelado = rwmb_meta('Tatigodoy_curso_parcelado');
		$curso_preco = rwmb_meta('Tatigodoy_curso_preco');
		$curso_link = rwmb_meta('Tatigodoy_curso_link');

		$parcelado_avista = rwmb_meta('Tatigodoy_curso_parcelado_avista');
		$curso_preco_avista = rwmb_meta('Tatigodoy_curso_preco_avista');
		$curso_link_avista = rwmb_meta('Tatigodoy_curso_link_avista');

		$curso_Whatsap = rwmb_meta('Tatigodoy_curso_Whatsap');
		$curso_promocao = rwmb_meta('Tatigodoy_curso_promocao');
		$curso_depoimentos = rwmb_meta('Tatigodoy_curso_depoimentos');
		$videosDepoimentos = $curso_depoimentos;
?>

		
	

<!-- TOPO -->
	<header>
		<div class="containerFull">
			<figure class="logo">
				<img src="<?php echo $configuracao['header_logo']['url'] ?>" alt="Logo">
			</figure>
			<h1><?php echo get_the_title(); ?></h1>
			<h2><?php echo $curso_subtitulo = rwmb_meta('Tatigodoy_curso_subtitulo');  ?></h2>
		</div>
	</header>

	<div class="pg-inicial">
		
		<!-- SOBRE O CURISO -->
		<section class="sobreCurso">
			<div class="containerFull">
				<h3 class="hidden"><?php echo get_the_title(); ?></h3>

				<article>
					<?php echo the_content(); ?>
				</article>

				<div class="areaDesc">
					<div class="row">
						<div class="col-sm-6">
							<div class="aresDescDois">
								<ul>
									<?php foreach ($curso_frase as $curso_frase): $curso_frase = $curso_frase ?>
									<li><p><?php echo $curso_frase   ?> <i class="fas fa-check"></i></p></li>
								<?php endforeach; ?>
							
								</ul>
								<a href="<?php echo get_permalink() ?>/#cursosPagamento" class="scrollTop">Inscreva-se agora</a>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="videoSobre">
								<div class="tagVideo">
									<iframe src="https://www.youtube.com/embed/<?php echo $curso_video = rwmb_meta('Tatigodoy_curso_video');  ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</section>

		

	</div>
	<?php  endwhile;endif;?>
<?php

get_footer();
