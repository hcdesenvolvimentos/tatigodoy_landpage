<?php
/**
 * Template Name: Inicial
 * @package tatigodoy
 */
global $configuracao;
get_header();
?>

<?php 
	$cursos = new WP_Query( array( 'post_type' => 'curso', 'posts_per_page' => 1 ));
	while($cursos->have_posts()): $cursos->the_post();
		$curso_frase = rwmb_meta('Tatigodoy_curso_frase');
		$curso_ganhos = rwmb_meta('Tatigodoy_curso_ganhos');
		$curso_endereco = rwmb_meta('Tatigodoy_curso_endereco');

		$curso_data = rwmb_meta('Tatigodoy_curso_data');
		$curso_material = rwmb_meta('Tatigodoy_curso_material');
		$curso_coffeesbreaks = rwmb_meta('Tatigodoy_curso_coffeesbreaks');

		// curso_preco
		$curso_parcelado = rwmb_meta('Tatigodoy_curso_parcelado');
		$curso_preco = rwmb_meta('Tatigodoy_curso_preco');
		$curso_link = rwmb_meta('Tatigodoy_curso_link');

		//curso_preco 2
		$curso_parcelado2 = rwmb_meta('Tatigodoy_curso_parcelado_novo');
		$curso_preco2 = rwmb_meta('Tatigodoy_curso_preco_novo');
		$curso_link2 = rwmb_meta('Tatigodoy_curso_link_novo');

		$parcelado_avista = rwmb_meta('Tatigodoy_curso_parcelado_avista');
		$curso_preco_avista = rwmb_meta('Tatigodoy_curso_preco_avista');
		$curso_link_avista = rwmb_meta('Tatigodoy_curso_link_avista');

		$curso_Whatsap = rwmb_meta('Tatigodoy_curso_Whatsap');
		$curso_promocao = rwmb_meta('Tatigodoy_curso_promocao');
		$curso_depoimentos = rwmb_meta('Tatigodoy_curso_depoimentos');
		$videosDepoimentos = $curso_depoimentos;

		$scriptFacebook = rwmb_meta('Tatigodoy_script_facebook');

		$titulo = get_the_title();
?>

	<!-- TOPO -->
	<header>
		<?php if($scriptFacebook){
			echo $scriptFacebook;	
		} ?>
		<div class="containerFull">
			<figure class="logo">
				<img src="<?php echo $configuracao['header_logo']['url'] ?>" alt="Logo">
			</figure>
			<h1><?php echo get_the_title(); ?></h1>
			<h2><?php echo $curso_subtitulo = rwmb_meta('Tatigodoy_curso_subtitulo');  ?></h2>
		</div>
	</header>

	<div class="pg-inicial">
		
		<!-- SOBRE O CURISO -->
		<section class="sobreCurso">
			<div class="containerFull">
				<h3 class="hidden"><?php echo get_the_title(); ?></h3>

				<article>
					<?php echo the_content(); ?>
				</article>

				<div class="areaDesc">
					<div class="row">
						<div class="col-sm-6">
							<div class="aresDescDois">
								<ul>
									<?php foreach ($curso_frase as $curso_frase): $curso_frase = $curso_frase ?>
									<li><p><?php echo $curso_frase   ?> <i class="fas fa-check"></i></p></li>
								<?php endforeach; ?>
							
								</ul>
								<a href="#cursosPagamento" class="scrollTop">Inscreva-se agora</a>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="videoSobre">
								<div class="tagVideo">
									<iframe src="https://www.youtube.com/embed/<?php echo $curso_video = rwmb_meta('Tatigodoy_curso_video');  ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</section>

		<section class="proximosCursos">
			<h4>Próximos cursos</h4>
			<?php 
				$pCursos = new WP_Query( array( 'post_type' => 'curso', 'posts_per_page' => -1 ));
				if($pCursos->have_posts()):

			?>
			<div class="carrosselProximosCursos">
				<div id="carrosselProximosCursos">
					<?php
						$contadorAlfinete = 0; 

						while ($pCursos->have_posts()):
						$pCursos->the_post();
						$tituloProximo = get_the_title();
						$prefixo = 'Tatigodoy_prox_curso_';
						
						$localProxCurso = rwmb_meta($prefixo.'local');
						$data = rwmb_meta($prefixo.'data');
						//$horaProximoCurso = rwmb_meta($prefixo.'hora');
						$fundo = rwmb_meta($prefixo.'fundo');
						$cidade= rwmb_meta($prefixo.'cidade');
						$tituloCurso = rwmb_meta($prefixo.'cidade');
						foreach ($fundo as $fundo) {
							$fundo = $fundo['full_url'];
						}
						$imagensDoLocal = rwmb_meta($prefixo.'imagens');

						if($titulo != $tituloProximo):
				 	?>
						<a href="<?php echo get_permalink(); ?>">	
							<div class="item" style="background: url(<?php echo $fundo ?>)">
								<?php ; 
									if($contadorAlfinete ==0){
										$alfinete = get_template_directory_uri() . '/img/11.png';
										$contadorAlfinete++;
									}else{
										$alfinete = get_template_directory_uri() . '/img/22.png';
									}
								?>
								<span class="alfinete" style="background: url(<?php echo $alfinete; ?>)"></span>
								<article class="">
									<h2 class="titulo"><?php echo $tituloCurso; ?></h2>
									<span class="dia"><strong>Dia:</strong> <?php echo $data; ?></span>
									<span class="local"><strong>Local:</strong> <?php echo $localProxCurso; ?></span>
									<!-- <span class="horario"><strong>Horário:</strong><?php //echo $horaProximoCurso; ?></span> -->
								</article>
								<div class="imagensLocal">
									<?php 
										$contador = 0;

										foreach ($imagensDoLocal as $imagensDoLocal):
											$imagemLocal = $imagensDoLocal['full_url']; 
											if($contador == 0):
									?>
											<img src="<?php echo $imagemLocal; ?>" alt="<?php echo $titulo; ?>" class="esquerda">
									<?php else: ?>
											<img src="<?php echo $imagemLocal; ?>" alt="<?php echo $titulo; ?>" class="direita">
									<?php endif; endforeach; ?>
								</div>
							</div>
						</a>
				<?php endif;  endwhile; wp_reset_query();  ?>
				</div>
			</div>
			<?php endif; ?>
		</section>

		<section class="cases">
			<h3 class="hidden">Cases</h3>
			<?php 
				$anos_experiencia = explode(",", $configuracao['anos_experiencia']);
				$texto_complementos = $configuracao['texto_complementos'];

				$cases_experiencia = explode(",", $configuracao['cases_experiencia']);
				$cases_complementos = $configuracao['cases_complementos'];

				$alunos_experiencia = explode(",", $configuracao['alunos_experiencia']);
				$alunos_complementos = $configuracao['alunos_complementos'];

				$cidades_experiencia = explode(",", $configuracao['cidades_experiencia']);
				$cidades_complementos = $configuracao['cidades_complementos'];
			?>
			<div class="casesLista">
				<ul>
					<li>
						<p> <small class="number" data-stop="<?php echo $anos_experiencia[0] ?>"></small>  <?php echo $anos_experiencia[1] ?></p>
						<span><?php echo $texto_complementos  ?></span>
					</li>

					<li>
						<p> <small class="number" data-stop="<?php echo $cases_experiencia[0] ?>"></small>  <?php echo $cases_experiencia[1] ?></p>
						<span><?php echo $cases_complementos  ?></span>
					</li>

					<li>
						<p> <small class="number" data-stop="<?php echo $alunos_experiencia[0] ?>"></small>  <?php echo $alunos_experiencia[1] ?></p>
						<span><?php echo $alunos_complementos  ?></span>
					</li>

					<li>
						<p> <small class="number" data-stop="<?php echo $cidades_experiencia[0] ?>"></small>  <?php echo $cidades_experiencia[1] ?></p>
						<span><?php echo $cidades_complementos  ?></span>
					</li>
				
				</ul>
			</div>
		</section>

		<section class="sobreTati">
			<?php 
				$tati_titulo = $configuracao['tati_titulo'];
				$tati_titulo_sub = $configuracao['tati_titulo_sub'];
				$tati_titulo_texto = $configuracao['tati_titulo_texto'];
				$tati_foto = $configuracao['tati_foto'];
			?>
			<h3 class="hidden"><?php echo $tati_titulo  ?></h3>

			<div class="containerFull">
				<div class="row">
					<div class="col-sm-3">
						<figure>
							<img src="<?php echo $tati_foto['url'] ?>" alt="<?php echo $tati_titulo ?>">
						</figure>
					</div>
					<div class="col-sm-9">
						<div class="areaTexto">
							<h2><?php echo $tati_titulo  ?></h2>
							<h3><?php echo $tati_titulo_sub  ?></h3>
							<p><?php echo $tati_titulo_texto ?></p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="quemFesRecomenda">
			<div class="containerFull">
				<h3>Quem fez, recomenda!</h3>
				
				<div class="row">
					<div class="col-sm-6">
						<ul>
							<?php
							$i = 0;
								foreach ($curso_depoimentos as $curso_depoimentos):
									$curso_depoimentos = explode("|", $curso_depoimentos);
									
							?>
							<li class="nomeVideos <?php if ($i == 0) { echo $ativo = "ativo";}?>" data-id="<?php echo $i ?>"><?php echo $curso_depoimentos[0] ?></li>
							<?php $i++;endforeach; ?>
							<li><a href="https://www.youtube.com/channel/UCBlBXUL1EuSFQX_u1LJ_yNw" target="_blank">Ver Mais</a></li>
						</ul>
					</div>
					<div class="col-sm-6">
						<div class="areaaVideo">
							<?php
								$j = 0;
								foreach ($videosDepoimentos as $videosDepoimentos):
									$videosDepoimentos = explode("|", $videosDepoimentos);
									
							?>
							<iframe id="<?php echo $j ?>" class="<?php if ($j == 0) {echo  $ativoVideo = "ativo";}; ?>" src="https://www.youtube.com/embed/<?php echo $videosDepoimentos[1] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							<?php  $j++;endforeach; ?>
						</div>
					</div>
				</div>
			</div>
			<a href="#cursosPagamento" class="linkInscricao scrollTop">Inscreva-se agora</a>
		</section>

		<section class="secaoDepoimentos">
			<h6 class="hidden">SEÇÃO DE DEPOIMENTOS</h6>
			<div class="lente"></div>
			<div class="containerFull">
				<div class="titulo">
					<h3><?php echo $configuracao['dept_titulo1'] ?></h3>
					<h3><?php echo $configuracao['dept_titulo2'] ?></h3>
				</div>
				<div class="row">
					<div class="depoimentos">

						<div id="btnCarrosselRight" class="btnCarrossel">
							<!-- <img src="<?php echo get_template_directory_uri() ?>/img/flechaDireita.png" alt=""> -->
							<i class="fas fa-angle-right"></i>
						</div>
						<div id="btnCarrosselLeft" class="btnCarrossel">
							<!-- <img src="<?php echo get_template_directory_uri() ?>/img/flechaEsquerda.png" alt=""> -->
							<i class="fas fa-angle-left"></i>
						</div>

						<?php

						$depoimentos = new WP_Query( array( 'post_type' => 'depoimento', 'orderby' => 'id', 'posts_per_page' => -1 ) );

						?>
						<div class="carrosselDepoimentos" id="carrosselDepoimentos">

							<?php

							while ($depoimentos->have_posts()): $depoimentos->the_post();
								$fotoDepoimento = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$fotoDepoimento = $fotoDepoimento[0];

							?>
							<div class="item">
								<div class="depoimento">
									<div class="texto">
										<p><?php customExcerpt(200); ?></p>
									</div>
									<div class="btnVerMais">
										<button data-nome="<?php echo get_the_title() ?>" data-content="<?php echo get_the_content(); ?>" data-img="<?php echo $fotoDepoimento ?>">ver mais</button>
									</div>
									<div class="imagem">
										<img src="<?php echo $configuracao['dept_img_baloon']['url'] ?>" alt="Imagem balão">
									</div>
									<figure>
										<?php if($fotoDepoimento): ?>
											<img src="<?php echo $fotoDepoimento; ?>" alt="Imagem depoimento">
										<?php else: ?>
											<img src="<?php echo get_template_directory_uri() ?>/img/girl.png" alt="Imagem depoimento">
										<?php endif; ?>
									</figure>
									<div class="nome">
										<p><?php echo get_the_title(); ?></p>
									</div>
								</div>
							</div>
							
							<?php endwhile; wp_reset_query();?>

						</div>
					</div>
				</div>
				
				<div class="popup">
					<div class="row">
						<span>X</span>
						<div class="col-sm-7">
							<div class="popup-texto">
								<p id="dept"></p>
							</div>
						</div>
						<div class="col-sm-5">
							<div class="popup-imagens">
								<figure class="balao">
									<img src="<?php echo get_template_directory_uri() ?>/img/baloon-virado.png" alt="">
								</figure>
								<div class="nomeFoto">
									<h3 id="nomeDept" class="nome"></h3>
									<figure class="foto">
										<img id="imgDept" src="" alt="">
									</figure>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>

		<section class="seusGanhos">
			<h3>Seus ganhos?</h3>
			
			<ul>
				<?php 
					$ganhos = new WP_Query( array( 'post_type' => 'ganhos', 'posts_per_page' => 4 ));
					while($ganhos->have_posts()): $ganhos->the_post();
						$icone = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$icone = $icone[0];
						$verificacao =  in_array($post->ID,$curso_ganhos);
						if ($verificacao):
				?>
				<li>
					<div class="ganho">
						<img src="<?php echo $icone  ?>" alt="<?php echo get_the_title() ?>">
						<h2><?php echo get_the_title() ?></h2>
						<div class="textoDesc">
							
							<?php echo the_content(); ?>
							
						</div>
					</div>
				</li>
				<?php endif;endwhile; wp_reset_query();?>

			</ul>
		</section>

		<section class="enderecoData">
			<h3 class="hidden">Endereços</h3>
			<div class="containerFull">
				<div class="row">
					<div class="col-sm-6">
						<div class="info">
							<div class="icon">
								<i class="fas fa-map-marker-alt"></i>
							</div>
							<a target="_blank" href="https://www.google.com.br/maps/place/<?php echo $curso_endereco  ?>"><?php echo $curso_endereco  ?> </a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="info">
							<div class="icon">
								<i class="fas fa-coffee"></i>
							</div>
							<p><?php echo $curso_coffeesbreaks  ?></p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<div class="info">
							<div class="icon">
								<i class="fas fa-calendar-alt"></i>
							</div>
							<p><?php echo $curso_data  ?></p>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="info">
							<div class="icon">
								<i class="fas fa-apple-alt"></i>
							</div>
							<p><?php echo $curso_material  ?></p>
						</div>
					</div>
				</div>
			</div>

		</section>

		<section class="cursosPagamento" id="cursosPagamento">
			<h3><?php echo $configuracao['pagamento_titulo'] ?></h3>
			<p><?php echo $configuracao['pagamento_titulo_subtitulo'] ?></p>

			<div class="planos">
				<ul>
					<?php if($configuracao['preco_grupos'] != 0): ?>
					<li>
						<div class="icon">
							<img src="<?php echo get_template_directory_uri() ?>/img/grupo.png" alt="CONFIRA O MELHOR PACOTE PARA VOCÊ E SEU GRUPO.">
						</div>
						<span>Grupos</span>
						<p class="texto"><small>CONFIRA O MELHOR PACOTE PARA VOCÊ E SEU GRUPO.  </small></p>
						<a href="<?php echo $curso_Whatsap ?>"  target="_blank">Entrar</a>
					</li>
				<?php endif; ?>
					
					<?php
					if($curso_preco):
					?>
					<li>
						<div class="icon">
							<img src="<?php echo get_template_directory_uri() ?>/img/card.png" alt="<?php echo $curso_parcelado ?>">
						</div>
						<span><?php echo $curso_parcelado ?></span>
						<p><small>R$</small> <?php echo $curso_preco ?></p>
						<a href="<?php echo $curso_link ?>" target="_blank" >compre agora</a>
					</li>
				<?php endif; 
				if($curso_preco_avista):
				?>
					<li>
						<div class="icon">
							<img src="<?php echo get_template_directory_uri() ?>/img/din.png" alt="<?php echo $parcelado_avista ?>">
						</div>
						<span><?php echo $parcelado_avista ?></span>
						<p><small>R$</small> <?php echo $curso_preco_avista ?></p>
						<a href="<?php echo $curso_link_avista ?>" target="_blank">compre agora</a>
					</li>
				<?php endif; ?>

				<?php if($curso_preco2): ?>
					<li>
						<div class="icon">
							<img src="<?php echo get_template_directory_uri() ?>/img/din.png" alt="<?php echo $curso_parcelado ?>">
						</div>
						<span><?php echo $curso_parcelado2 ?></span>
						<p><small>R$</small> <?php echo $curso_preco2 ?></p>
						<a href="<?php echo $curso_link2 ?>" target="_blank" >compre agora</a>
					</li>
					<?php endif; ?>

				</ul>
			</div>

			<?php if($configuracao['contador'] != 0): ?>
				<div class="contadorArea" id="diasParaoCurso" data-diasParaoCurso="<?php echo $curso_promocao ?>">
					<span>O TEMPO ESTÁ ACABANDO!!!</span>
					<div class="contador">
						<div class="cont">
							<h3 id="dias" class="numeros">0</h3>
							<small>Dias</small>
						</div>
						<div class="cont">
							<h3 id="horas" class="numeros">0</h3>
							<small>Horas</small>
						</div>
						<div class="cont">
							<h3 id="minutos" class="numeros">0</h3>
							<small>Minutos</small>
						</div>
						<div class="cont">
							<h3 id="segundos" class="numeros">0</h3>
							<small>Segundos</small>
						</div>
						<div class="cont">
							<h3 id="milisegundos" class="numeros">0</h3>
							<small>Milissegundos</small>
						</div>
					</div>

				</div>
			<?php endif; ?>

		</section>

		<section class="areaBlog">
			<h3>Conheça o blog da Tati!</h3>

			<div class="posts">
				<ul>
					<li id="0">
						<article>
							<a href="" target="_blank">
								<div class="postHover" style="background: #309498bd;">
									<span class="dataPostHover"></span>
									<h1 class="tituloPostHover"></h1>
									<p class="descricaoPostHover"></p>
									<img src="http://tatigodoy.hcdesenvolvimentos.com.br/wp-content/uploads/2018/10/iconePost1.png" alt="Tati Godoy blog">
								</div>	
								<figure class="imagemDestaquePost" style="">
									<img src="" alt="Curso Personal Organizer – São Paulo – 2019">
								</figure>
								<div class="informacoesPost" style="background: #309498bd">
									<div class="row">
										<div class="">
											<div class="detalhesPost">
												<h1 class="tituloPost"></h1>
												<h4 class="dataPost"></h4>
											</div>
										</div>
									</div>
								</div>
							</a>
						</article>
					</li>
					<li id="1">
						<article>
							<a href="" target="_blank">
								<div class="postHover" style="background: #309498bd;">
									<span class="dataPostHover"></span>
									<h1 class="tituloPostHover"></h1>
									<p class="descricaoPostHover"></p>
									<img src="http://tatigodoy.hcdesenvolvimentos.com.br/wp-content/uploads/2018/10/iconePost1.png" alt="Tati Godoy blog">
								</div>	
								<figure class="imagemDestaquePost" style="">
									<img src="" alt="Curso Personal Organizer – São Paulo – 2019">
								</figure>
								<div class="informacoesPost" style="background: #309498bd">
									<div class="row">
										<div class="">
											<div class="detalhesPost">
												<h1 class="tituloPost"></h1>
												<h4 class="dataPost"></h4>
											</div>
										</div>
									</div>
								</div>
							</a>
						</article>
					</li>
					<li id="2">
						<article>
							<a href="" target="_blank">
								<div class="postHover" style="background: #309498bd;">
									<span class="dataPostHover"></span>
									<h1 class="tituloPostHover"></h1>
									<p class="descricaoPostHover"></p>
									<img src="http://tatigodoy.hcdesenvolvimentos.com.br/wp-content/uploads/2018/10/iconePost1.png" alt="Tati Godoy blog">
								</div>	
								<figure class="imagemDestaquePost" style="">
									<img src="" alt="Curso Personal Organizer – São Paulo – 2019">
								</figure>
								<div class="informacoesPost" style="background: #309498bd">
									<div class="row">
										<div class="">
											<div class="detalhesPost">
												<h1 class="tituloPost"></h1>
												<h4 class="dataPost"></h4>
											</div>
										</div>
									</div>
								</div>
							</a>
						</article>
					</li>
				</ul>
			</div>
			
		</section>

	</div>
<?php endwhile; wp_reset_query();?>

<?php get_footer(); ?>