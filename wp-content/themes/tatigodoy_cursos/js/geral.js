$(function(){
   // inicializando com a data
   var contagemRegressiva = new Date("Dec 26, 2018 00:00:00").getTime();

   //ativando a funÃ§Ã£o a cada um segundo
   var x = setInterval(function() {

   // pegando o horÃ¡rio atual
   var hoje = new Date().getTime();

   // subtraindo o tempo que resta entre a data escolhida e hoje
   var tempoRestante = contagemRegressiva - hoje;

   // contas pegas na internet para conseguir calcular os dias, horas, minutos e segundos
   var dias = Math.floor(tempoRestante / (1000 * 60 * 60 * 24));
   var horas = Math.floor((tempoRestante % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
   var minutos = Math.floor((tempoRestante % (1000 * 60 * 60)) / (1000 * 60));
   var segundos = Math.floor((tempoRestante % (1000 * 60)) / 1000);
   var miliSegundos = (((tempoRestante % (1000 * 60)) / 1000) - segundos).toFixed(2).replace(/[^\d]+/g,'').replace(0,'');

  if (segundos != 0 && segundos > -1) {

           
                // colcando no html na id "contador"
                document.getElementById('dias').innerHTML = dias;
                document.getElementById('horas').innerHTML = horas;
                document.getElementById('minutos').innerHTML = minutos;
                document.getElementById('segundos').innerHTML = segundos;
                document.getElementById('milisegundos').innerHTML = miliSegundos;
               
            }else if (dias == 0 && horas == 0 && minutos == 0 && segundos == 0 && miliSegundos == 0) {
                // colcando no html na id "contador"
                document.getElementById('dias').innerHTML  = 0;
                document.getElementById('horas').innerHTML  = 0;
                document.getElementById('minutos').innerHTML  = 0;
                document.getElementById('segundos').innerHTML = 0;
                document.getElementById('milisegundos').innerHTML  = 0;
               
                
            
            }

       }, 1);

    $.fn.isOnScreen = function(){
       var win = $(window);
       var viewport = {
           top : win.scrollTop(),
           left : win.scrollLeft()
       };

       viewport.right = viewport.left + win.width();
       viewport.bottom = viewport.top + win.height();

       var bounds = this.offset();
       bounds.right = bounds.left + this.outerWidth();
       bounds.bottom = bounds.top + this.outerHeight();

       return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
   };

   var testeMostrarVisita = false;
   $(window).scroll(function(){

       if ($('.cases').isOnScreen() == true && testeMostrarVisita == false) {

          testeMostrarVisita = true;
          setTimeout(function(){
              $(".cases  small.number").each(function() {
                  var o = $(this);
                   $({Counter: 0}).animate({
                       Counter: o.attr("data-stop")
                   },{
                       duration: 5e3,
                       easing: "swing",
                       step: function(e) {
                           o.text(Math.ceil(e))
                       }
                   })
               });
               $({Counter: 0}).animate({
                   Counter: o.attr("data-stop")
               },{
                   duration: 5e3,
                   easing: "swing",
                   step: function(e) {
                       o.text(Math.ceil(e))
                   }
               });
           }, 1);

       }
   });

   $('a.scrollTop').click(function() {
       if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
           var target = $(this.hash);
           target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
               $('html,body').animate({
                   scrollTop: target.offset().top
               }, 1000);
               return false;
           }
       }

   });



   // SCRIPTS HEIGHT 100% MODAL
   $(window).bind('scroll', function () {
      var alturaScroll = $(window).scrollTop()
      if (alturaScroll > 50) {
           $("header").addClass("topoFixed");
      }else{
           $("header").removeClass("topoFixed");
      }
   });

   
  $('.pg-inicial .quemFesRecomenda ul li').click(function(){
    let idVideo = $(this).attr('data-id');
    $(this).removeClass('ativo');
    $('.pg-inicial .quemFesRecomenda .areaaVideo iframe').removeClass('ativo');
    $('.pg-inicial .quemFesRecomenda .areaaVideo iframe#'+idVideo).addClass('ativo');
    $('.pg-inicial .quemFesRecomenda ul li').removeClass('ativo');
    $(this).addClass('ativo');
  });

  $('.pg-inicial .secaoDepoimentos .depoimentos .depoimento .btnVerMais button').click(function(e){
    $('.pg-inicial .secaoDepoimentos .popup').addClass('show');
    var dataNome = $(this).attr('data-nome');
    var dataContent = $(this).attr('data-content');
    var dataImage = $(this).attr('data-img');
    $('#nomeDept').text($(this).attr('data-nome'));
    $('#dept').text($(this).attr('data-content'));
    $('#imgDept').attr('src', dataImage);
    $('.pg-inicial .secaoDepoimentos .lente').css("visibility", "visible");
    $('body').attr('onwheel', 'return false');
  });

  $('.pg-inicial .secaoDepoimentos .popup span').click(function(){
    $('.pg-inicial .secaoDepoimentos .popup').removeClass('show');
    $('.pg-inicial .secaoDepoimentos .lente').css("visibility", "hidden");
    $('body').attr('onwheel', 'return true');
  });

  $('.pg-inicial .secaoDepoimentos .lente').click(function(){
    $('.pg-inicial .secaoDepoimentos .popup').removeClass('show');
    $('.pg-inicial .secaoDepoimentos .lente').css("visibility", "hidden");
    $('body').attr('onwheel', 'return true');
  });

  $("#carrosselDepoimentos").owlCarousel({
    items: 3,
    dots: !0,
    loop: 0,
    lazyLoad: !0,
    mouseDrag: true,
    touchDrag: 1,
    autoplay: true,
    autoplayTimeout: 2e3,
    autoplayHoverPause: !0,
    animateOut: "fadeOut",
    smartSpeed: 450,

            //CARROSSEL RESPONSIVO
            responsiveClass:true,
            responsive:{
              320:{
                items:1
              },

              600:{
                items:1
              },

              991:{
                items:2
              },

              1024:{
                items:3
              },
              
              1440:{
                items:3
              },
              
              1920: {
                items:3
              },

            }
          })

   //BOTÕES DO CARROSSEL DE PARCERIA
   var carrossel_depoimentosEsquerda = $("#carrosselDepoimentos").data('owlCarousel');
   $('#btnCarrosselLeft').click(function(){
     carrossel_depoimentosEsquerda.prev();
   });
   var carrossel_depoimentosDireita = $("#carrosselDepoimentos").data('owlCarousel');
   $('#btnCarrosselRight').click(function(){
     carrossel_depoimentosDireita.next();
   });

    $.ajax({
    type:'GET',
    url: 'http://www.tatigodoy.com.br/wp-json/wp/v2/posts?_embed',
    success:function(posts){
       console.log(posts);
       i = 0;
       while(i <= 2) {
          var id = posts[i].id;
          var img = posts[i]._embedded['wp:featuredmedia']['0'].source_url;
          var link = posts[i].link;
          var excerpt = posts[i].excerpt.rendered;   
          var reExcerpt = excerpt.substring(3,100) + "...";
          var titulo = posts[i].title.rendered;
          var data = posts[i].date;
          var ano = data.substring(0, 4);
          var mes = data.substring(5, 7);
          var dia = data.substring(8, 10);

          switch(mes){
            case '01':
              mes = 'JAN'; break;
            case '02':
              mes = 'FEV'; break;
            case '03':
              mes = 'MAR'; break;
            case '04':
              mes = 'ABR'; break;
            case '05':
              mes = 'MAI'; break;
            case '06':
              mes = 'JUN'; break;
            case '07':
              mes = 'JUL'; break;
            case '08':
              mes = 'AGO'; break;
            case '09':
              mes = 'SET'; break;
            case '10':
              mes = 'OUT'; break;
            case '11':
              mes = 'NOV'; break;
            case '12':
              mes = 'DEZ'; break;
          }
          var dataFormat = dia + ' ' + mes + ' ' + ano;
          // var tituloFormat = titulo.replace('&#8211;', '');

          $('.posts ul li#'+ i +' article .informacoesPost .detalhesPost h1.tituloPost').text(titulo);
          $('.posts ul li#'+ i +' article .postHover .tituloPostHover').text(titulo);
          $('.posts ul li#'+ i +' article .postHover .descricaoPostHover').text(reExcerpt);
          $('.posts ul li#'+ i +' article .postHover .dataPostHover').text(dataFormat);
          $('.posts ul li#'+ i +' article .informacoesPost .detalhesPost h4.dataPost').text(dataFormat);
          $('.posts ul li#'+ i +' article a').attr('href',link);
          $('.posts ul li#'+ i +' article a .imagemDestaquePost').css({'background' : 'url('+ img +')'});
          $('.posts ul li#'+ i +' article a .imagemDestaquePost img').attr('src',img);
          console.log(titulo);
          // $('.posts ul').append();

          $('#titulo').text(titulo);
          i++;
       }
    }
  })

});