<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Tati_Godoy_Cursos
 */
global $configuracao;
get_header();
?>

<?php 
	if ( have_posts() ) : while ( have_posts() ) :the_post();
		$curso_frase = rwmb_meta('Tatigodoy_curso_frase');
		$curso_ganhos = rwmb_meta('Tatigodoy_curso_ganhos');
		$curso_endereco = rwmb_meta('Tatigodoy_curso_endereco');

		$curso_data = rwmb_meta('Tatigodoy_curso_data');
		$curso_material = rwmb_meta('Tatigodoy_curso_material');
		$curso_coffeesbreaks = rwmb_meta('Tatigodoy_curso_coffeesbreaks');

		// curso_preco
		$curso_parcelado = rwmb_meta('Tatigodoy_curso_parcelado');
		$curso_preco = rwmb_meta('Tatigodoy_curso_preco');
		$curso_link = rwmb_meta('Tatigodoy_curso_link');

		$parcelado_avista = rwmb_meta('Tatigodoy_curso_parcelado_avista');
		$curso_preco_avista = rwmb_meta('Tatigodoy_curso_preco_avista');
		$curso_link_avista = rwmb_meta('Tatigodoy_curso_link_avista');

		$curso_Whatsap = rwmb_meta('Tatigodoy_curso_Whatsap');
		$curso_promocao = rwmb_meta('Tatigodoy_curso_promocao');
?>

		
	

<!-- TOPO -->
	<header>
		<div class="containerFull">
			<figure class="logo">
				<img src="<?php echo $configuracao['header_logo']['url'] ?>" alt="Logo">
			</figure>
			<h1><?php echo get_the_title(); ?></h1>
			<h2><?php echo $curso_subtitulo = rwmb_meta('Tatigodoy_curso_subtitulo');  ?></h2>
		</div>
	</header>

	<div class="pg-inicial">
		
		<!-- SOBRE O CURISO -->
		<section class="sobreCurso">
			<div class="containerFull">
				<h3 class="hidden"><?php echo get_the_title(); ?></h3>

				<article>
					<?php echo the_content(); ?>
				</article>

				<div class="areaDesc">
					<div class="row">
						<div class="col-sm-6">
							<div class="aresDescDois">
								<ul>
									<?php foreach ($curso_frase as $curso_frase): $curso_frase = $curso_frase ?>
									<li><p><?php echo $curso_frase   ?> <i class="fas fa-check"></i></p></li>
								<?php endforeach; ?>
							
								</ul>
								<a href="#cursosPagamento" class="scrollTop">Inscreva-se agora</a>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="videoSobre">
								<div class="tagVideo">
									<iframe src="https://www.youtube.com/embed/<?php echo $curso_video = rwmb_meta('Tatigodoy_curso_video');  ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</section>

		<section class="cases">
			<h3 class="hidden">Cases</h3>
			<?php 
				$anos_experiencia = explode(",", $configuracao['anos_experiencia']);
				$texto_complementos = $configuracao['texto_complementos'];

				$cases_experiencia = explode(",", $configuracao['cases_experiencia']);
				$cases_complementos = $configuracao['cases_complementos'];

				$alunos_experiencia = explode(",", $configuracao['alunos_experiencia']);
				$alunos_complementos = $configuracao['alunos_complementos'];

				$cidades_experiencia = explode(",", $configuracao['cidades_experiencia']);
				$cidades_complementos = $configuracao['cidades_complementos'];
			?>
			<div class="casesLista">
				<ul>
					<li>
						<p> <small class="number" data-stop="<?php echo $anos_experiencia[0] ?>"></small>  <?php echo $anos_experiencia[1] ?></p>
						<span><?php echo $texto_complementos  ?></span>
					</li>

					<li>
						<p> <small class="number" data-stop="<?php echo $cases_experiencia[0] ?>"></small>  <?php echo $cases_experiencia[1] ?></p>
						<span><?php echo $cases_complementos  ?></span>
					</li>

					<li>
						<p> <small class="number" data-stop="<?php echo $alunos_experiencia[0] ?>"></small>  <?php echo $alunos_experiencia[1] ?></p>
						<span><?php echo $alunos_complementos  ?></span>
					</li>

					<li>
						<p> <small class="number" data-stop="<?php echo $cidades_experiencia[0] ?>"></small>  <?php echo $cidades_experiencia[1] ?></p>
						<span><?php echo $cidades_complementos  ?></span>
					</li>
				
				</ul>
			</div>
		</section>

		<section class="sobreTati">
			<?php 
				$tati_titulo = $configuracao['tati_titulo'];
				$tati_titulo_sub = $configuracao['tati_titulo_sub'];
				$tati_titulo_texto = $configuracao['tati_titulo_texto'];
				$tati_foto = $configuracao['tati_foto'];
			?>
			<h3 class="hidden"><?php echo $tati_titulo  ?></h3>

			<div class="containerFull">
				<div class="row">
					<div class="col-sm-3">
						<figure>
							<img src="<?php echo $tati_foto['url'] ?>" alt="<?php echo $tati_titulo ?>">
						</figure>
					</div>
					<div class="col-sm-9">
						<div class="areaTexto">
							<h2><?php echo $tati_titulo  ?></h2>
							<h3><?php echo $tati_titulo_sub  ?></h3>
							<p><?php echo $tati_titulo_texto ?></p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="quemFesRecomenda">
			<div class="containerFull">
				<h3>Quem fez, recomenda!</h3>
				
				<div class="row">
					<div class="col-sm-6">
						<ul>
							<li>Depoimentos sobre o Curso de Personal</li>
							<li>Testemunhos Sobre Tati Godoy</li>
							<li>Depoimentos sobre o Curso de Personal</li>
							<li>Testemunhos Sobre Tati Godoy</li>
							<li>Depoimentos sobre o Curso de Personal</li>
							<li>Testemunhos Sobre Tati Godoy</li>
							<li><a href="#">Ver Mais</a></li>
						</ul>
					</div>
					<div class="col-sm-6">
						<div class="areaaVideo">
							<iframe src="https://www.youtube.com/embed/Hwu8UQ36IOc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
			<a href="#cursosPagamento" class="linkInscricao scrollTop">Inscreva-se agora</a>
		</section>

		<section class="seusGanhos">
			<h3>Seus ganhos?</h3>
			
			<ul>
				<?php 
					$ganhos = new WP_Query( array( 'post_type' => 'ganhos', 'posts_per_page' => 4 ));
					while($ganhos->have_posts()): $ganhos->the_post();
						$icone = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$icone = $icone[0];
						$verificacao =  in_array($post->ID,$curso_ganhos);
						if ($verificacao):
				?>
				<li>
					<div class="ganho">
						<img src="<?php echo $icone  ?>" alt="<?php echo get_the_title() ?>">
						<h2><?php echo get_the_title() ?></h2>
						<div class="textoDesc">
							
							<?php echo the_content(); ?>
							
						</div>
					</div>
				</li>
				<?php endif;endwhile; wp_reset_query();?>

			</ul>
		</section>

		<section class="enderecoData">
			<h3 class="hidden">Endereços</h3>
			<div class="containerFull">
				<div class="row">
					<div class="col-sm-6">
						<div class="info">
							<div class="icon">
								<i class="fas fa-map-marker-alt"></i>
							</div>
							<a target="_blank" href="https://www.google.com.br/maps/place/<?php echo $curso_endereco  ?>"><?php echo $curso_endereco  ?> </a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="info">
							<div class="icon">
								<i class="fas fa-coffee"></i>
							</div>
							<p><?php echo $curso_coffeesbreaks  ?></p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<div class="info">
							<div class="icon">
								<i class="fas fa-calendar-alt"></i>
							</div>
							<p><?php echo $curso_data  ?></p>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="info">
							<div class="icon">
								<i class="fas fa-apple-alt"></i>
							</div>
							<p><?php echo $curso_material  ?></p>
						</div>
					</div>
				</div>
			</div>

		</section>

		<section class="cursosPagamento" id="cursosPagamento">
			<h3><?php echo $configuracao['pagamento_titulo'] ?></h3>
			<p><?php echo $configuracao['pagamento_titulo_subtitulo'] ?></p>

			<div class="planos">
				<ul>
					<li>
						<div class="icon">
							<img src="<?php echo get_template_directory_uri() ?>/img/card.png" alt="<?php echo $curso_parcelado ?>">
						</div>
						<span><?php echo $curso_parcelado ?></span>
						<p><small>R$</small> <?php echo $curso_preco ?></p>
						<a href="<?php echo $curso_link ?>" target="_blank" >compre agora</a>
					</li>

					<li>
						<div class="icon">
							<img src="<?php echo get_template_directory_uri() ?>/img/din.png" alt="<?php echo $parcelado_avista ?>">
						</div>
						<span><?php echo $parcelado_avista ?></span>
						<p><small>R$</small> <?php echo $curso_preco_avista ?></p>
						<a href="<?php echo $curso_link_avista ?>" target="_blank">compre agora</a>
					</li>

					<li>
						<div class="icon">
							<img src="<?php echo get_template_directory_uri() ?>/img/grupo.png" alt="CONFIRA O MELHOR PACOTE PARA VOCÊ E SEU GRUPO.">
						</div>
						<span>Grupos</span>
						<p class="texto"><small>CONFIRA O MELHOR PACOTE PARA VOCÊ E SEU GRUPO.  </small></p>
						<a href="<?php echo $curso_Whatsap ?>"  target="_blank">compre agora</a>
					</li>

				</ul>
			</div>

			<div class="contadorArea" id="diasParaoCurso" data-diasParaoCurso="<?php echo $curso_promocao ?>">
				<span>FINAL DA PROMOÇÂO:</span>
				<div class="contador">
					<div class="cont">
						<h3 id="dias" class="numeros">0</h3>
						<small>Dias</small>
					</div>
					<div class="cont">
						<h3 id="horas" class="numeros">0</h3>
						<small>Horas</small>
					</div>
					<div class="cont">
						<h3 id="minutos" class="numeros">0</h3>
						<small>Minutos</small>
					</div>
					<div class="cont">
						<h3 id="segundos" class="numeros">0</h3>
						<small>Segundos</small>
					</div>
					<div class="cont">
						<h3 id="milisegundos" class="numeros">0</h3>
						<small>Milissegundos</small>
					</div>
				</div>

			</div>
		</section>

		<section class="areaBlog">
			<h3>Conheça o blog da Tati!</h3>

			<div class="posts">
				<ul>
					<li>
						<article>
							<a href="http://www.tatigodoy.com.br/curso-personal-organizer-sao-paulo-2019/">
								<div class="postHover" style="background: #309498bd ;">
									<span class="dataPostHover">31 ago 2018</span>
									<h1 class="tituloPostHover">Curso Personal Organizer – São Paulo – 2019</h1>
									<p class="descricaoPostHover">Garanta sua vaga aqui!&nbsp;</p>
									<img src="http://tatigodoy.hcdesenvolvimentos.com.br/wp-content/uploads/2018/10/iconePost1.png" alt="Curso Personal Organizer – São Paulo – 2019">
								</div>	
								<figure class="imagemDestaquePost" style="background: url(http://www.tatigodoy.com.br/wp-content/uploads/2017/09/IMG_1799.jpg)">
									<img src="http://www.tatigodoy.com.br/wp-content/uploads/2017/09/IMG_1799.jpg" alt="Curso Personal Organizer – São Paulo – 2019">
								</figure>
								<div class="informacoesPost" style="background: #309498bd ">
									<div class="row">
										<div class="col-xs-3">
											<div class="iconePost">
												<img src="http://tatigodoy.hcdesenvolvimentos.com.br/wp-content/uploads/2018/10/iconePost1.png" alt="Curso Personal Organizer – São Paulo – 2019">
											</div>
										</div>
										<div class="col-xs-9">
											<div class="detalhesPost">
												<h1 class="tituloPost">Curso Personal Organizer – São Paulo – 2019</h1>
												<h4 class="dataPost">31 ago 2018</h4>
											</div>
										</div>
									</div>
								</div>
							</a>
						</article>
					</li>
					<li>
						<article>
							<a href="http://www.tatigodoy.com.br/curso-personal-organizer-sao-paulo-2019/">
								<div class="postHover" style="background: #309498bd ;">
									<span class="dataPostHover">31 ago 2018</span>
									<h1 class="tituloPostHover">Curso Personal Organizer – São Paulo – 2019</h1>
									<p class="descricaoPostHover">Garanta sua vaga aqui!&nbsp;</p>
									<img src="http://tatigodoy.hcdesenvolvimentos.com.br/wp-content/uploads/2018/10/iconePost1.png" alt="Curso Personal Organizer – São Paulo – 2019">
								</div>	
								<figure class="imagemDestaquePost" style="background: url(http://www.tatigodoy.com.br/wp-content/uploads/2017/09/IMG_1799.jpg)">
									<img src="http://www.tatigodoy.com.br/wp-content/uploads/2017/09/IMG_1799.jpg" alt="Curso Personal Organizer – São Paulo – 2019">
								</figure>
								<div class="informacoesPost" style="background: #309498bd ">
									<div class="row">
										<div class="col-xs-3">
											<div class="iconePost">
												<img src="http://tatigodoy.hcdesenvolvimentos.com.br/wp-content/uploads/2018/10/iconePost1.png" alt="Curso Personal Organizer – São Paulo – 2019">
											</div>
										</div>
										<div class="col-xs-9">
											<div class="detalhesPost">
												<h1 class="tituloPost">Curso Personal Organizer – São Paulo – 2019</h1>
												<h4 class="dataPost">31 ago 2018</h4>
											</div>
										</div>
									</div>
								</div>
							</a>
						</article>
					</li>
					<li>
						<article>
							<a href="http://www.tatigodoy.com.br/curso-personal-organizer-sao-paulo-2019/">
								<div class="postHover" style="background: #309498bd ;">
									<span class="dataPostHover">31 ago 2018</span>
									<h1 class="tituloPostHover">Curso Personal Organizer – São Paulo – 2019</h1>
									<p class="descricaoPostHover">Garanta sua vaga aqui!&nbsp;</p>
									<img src="http://tatigodoy.hcdesenvolvimentos.com.br/wp-content/uploads/2018/10/iconePost1.png" alt="Curso Personal Organizer – São Paulo – 2019">
								</div>	
								<figure class="imagemDestaquePost" style="background: url(http://www.tatigodoy.com.br/wp-content/uploads/2017/09/IMG_1799.jpg)">
									<img src="http://www.tatigodoy.com.br/wp-content/uploads/2017/09/IMG_1799.jpg" alt="Curso Personal Organizer – São Paulo – 2019">
								</figure>
								<div class="informacoesPost" style="background: #309498bd ">
									<div class="row">
										<div class="col-xs-3">
											<div class="iconePost">
												<img src="http://tatigodoy.hcdesenvolvimentos.com.br/wp-content/uploads/2018/10/iconePost1.png" alt="Curso Personal Organizer – São Paulo – 2019">
											</div>
										</div>
										<div class="col-xs-9">
											<div class="detalhesPost">
												<h1 class="tituloPost">Curso Personal Organizer – São Paulo – 2019</h1>
												<h4 class="dataPost">31 ago 2018</h4>
											</div>
										</div>
									</div>
								</div>
							</a>
						</article>
					</li>

				</ul>
			</div>
			
		</section>

	</div>
	<?php  endwhile;endif;?>
<?php

get_footer();
